from flask import Flask, request

app = Flask(__name__)
jobs = list()


def print_jobs_list(jobs: list) -> str:
    jobs_pretty = 'Available jobs:<br>'
    if len(jobs) == 0:
        return jobs_pretty + '<br>' + 'No jobs, come back later!'
    for job in jobs:
        jobs_pretty += job + '\n'
    return jobs_pretty


@app.route('/')
def view_jobs() -> str:
    result = 'Indeed Jr<br>'
    return result + '<br>' + print_jobs_list(jobs)


@app.route('/post_job')
def post_job() -> str:
    job = request.args.get('title')
    if job:
        jobs.append(job)
        return f'Added job {job}!'
    else:
        return 'sorry, make sure you include title as a param'


app.run()
