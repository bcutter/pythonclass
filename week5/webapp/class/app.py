from flask import Flask, request

app = Flask(__name__)

jobs = ['Accountant']

def print_jobs_list(jobs: list) -> str:
    jobs_pretty = 'Available jobs:<br>'
    if len(jobs) == 0:
        return jobs_pretty + '<br>' + 'No jobs, come back later!'
    for job in jobs:
        jobs_pretty += job + '<br>'
    return jobs_pretty

@app.route('/')
def job_list() -> str:
    return print_jobs_list(jobs)

@app.route('/post')
def post_job():
    job = request.args.get('title', '')
    if job:
        jobs.append(job)
        return f'Added job {job}'
    else:
        return 'Could not add job, please include title parameter'


app.run()

