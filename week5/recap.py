import copy

def list_references_shallow():
    first_list = [1, 2, 3]
    second_list = first_list

    if (second_list is first_list):
        print('Twins!')

def list_reference_deep():
    first_list = [1, 2, 3]
    second_list = copy.deepcopy(first_list)
    if (second_list is first_list):
        print('Twins!')
    else:
        print('Nope')

# Recap One: copying a list is a SHALLOW COPY
#list_references_shallow()

list_reference_deep()