from flask import Flask, render_template
from flask_wtf import FlaskForm
from wtforms import StringField
from wtforms.validators import DataRequired
from datetime import datetime

class JobForm(FlaskForm):
    title = StringField('title', validators=[DataRequired()])
    location = StringField('location', validators=[DataRequired()])

class Job:
    def __init__(self, title: str, location: str):
        self.title = title
        self.location = location
        self.date_posted = datetime.now()

class Employer:
    def __init__(self, company_name: str):
        self.company_name = company_name
        self.jobs = list()

    def add_job(self, job: Job):
        self.jobs = job




app = Flask(__name__)

# In order to use CSRF, we need a secret key.  Don't worry about this for now
app.secret_key = 'abcd'

job_list: [Job] = list()


@app.route('/', methods=('GET', 'POST'))
def handle_form():
    form = JobForm()
    if form.validate_on_submit():
        title = form.title.data
        location = form.location.data
        job_list.append(Job(title, location))
    return render_template('form.html', form=form, jobs=job_list)


app.run()