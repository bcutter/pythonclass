from flask import Flask, render_template
from flask_wtf import FlaskForm
from wtforms import StringField
from wtforms.validators import DataRequired
from datetime import datetime
import csv

class JobForm(FlaskForm):
    title = StringField('title', validators=[DataRequired()])
    location = StringField('location', validators=[DataRequired()])

class Job:
    def __init__(self, title: str, location: str):
        self.title = title
        self.location = location
        self.date_posted = datetime.now()

    def __init__(self, title: str, location: str, date_posted: str):
        self.title = title
        self.location = location
        self.date_posted = date_posted
class Employer:
    def __init__(self, company_name: str):
        self.company_name = company_name
        self.jobs = list()

    def add_job(self, job: Job):
        self.jobs = job

JOB_CSV_NAME = './jobs.csv'

def write_new_job(job: Job):
    with open(JOB_CSV_NAME, 'a') as csv_file:
        writer = csv.writer(csv_file)
        writer.writerow([job.title, job.location, job.date_posted])

def read_jobs():
    with open(JOB_CSV_NAME, 'r') as csv_file:
        reader = csv.reader(csv_file)
        job_list = list()
        for row in reader:
            job_list.append(Job(row[0], row[1], row[2]))
        return job_list

app = Flask(__name__)

# In order to use CSRF, we need a secret key.  Don't worry about this for now
app.secret_key = 'abcd'

job_list: [Job] = list()


@app.route('/', methods=('GET', 'POST'))
def handle_form():
    form = JobForm()
    if form.validate_on_submit():
        title = form.title.data
        location = form.location.data
        new_job = Job(title, location)
        job_list.append(new_job)
        write_new_job(new_job)
    return render_template('form.html', form=form, jobs=read_jobs())


app.run()