romeo = 0
juliet = 0

with open('./resources/shakespeare.txt') as book:
    for line in book:
        lower_case = line.lower()
        if 'romeo' in lower_case:
            romeo += 1
        if 'juliet' in lower_case:
            juliet += 1

print(f'Romeo appeared in {romeo} lines, and Juliet appeared in {juliet} lines')