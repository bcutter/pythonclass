beer = 99

with open('./resources/bottles.text', 'w+') as bottes_file:
    while beer > 0:
        bottes_file.write(f'{beer} bottles of beer on the wall, {beer}! Take one down, pass it around, {beer - 1} bottles of beer on the wall\n')
        beer -= 1