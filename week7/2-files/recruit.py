import csv

last_printed = 0
last_evaluated = 0
starting_val = 0
with open('./resources/recruit.csv') as csv_file:
    reader = csv.reader(csv_file)
    for row in reader:
        date, open_val, high, low, close, adj_close, volume = row
        try:
            close_float = float(close)
            volume_float = float(volume)
            total_val = close_float * volume_float
            visual = '' if starting_val == 0 else ''.join(["|" for _ in range(int((close_float - starting_val) / 10))])
            if close_float > last_printed + 100:
                print(f'On {date}, Recruit traded at {close} for a total value of ${total_val}  {visual}')
                last_printed = close_float
            if starting_val == 0:
                starting_val = close_float
            last_evaluated = close_float
        except ValueError:
            pass

growth = (last_printed - starting_val) / starting_val
print(f'Recruit share price increased by {growth * 100}%')
