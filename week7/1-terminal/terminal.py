import os
import shutil

original_dir = '../0-assignment'
target_dir = '../indeedjr'


if target_dir[3:] not in os.listdir('..'):
    shutil.copytree(original_dir, target_dir)
else:
    shutil.rmtree('../indeedjr', ignore_errors=True)