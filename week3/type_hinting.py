def square_number(number: int):
    return number * number


square_number(2)

#square_number('test')


def add_numbers(numbers: list[int]) -> int:
    sum = 0
    for num in numbers:
        sum += num
    return sum


print(add_numbers([1, 8, 9]))