import os

cur_dir = os.getcwd()
new_dir = f'{cur_dir}/cool_project'
os.mkdir(new_dir)

os.rmdir(new_dir)

def do_processing():
    return 0

def write_to_csv(data):
    pass

def write_to_db(data):
    pass

data = do_processing()
write_to_db(data)
write_to_csv(data)

