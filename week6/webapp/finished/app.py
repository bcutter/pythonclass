from flask import Flask, request, render_template

app = Flask(__name__)
jobs = ['accountant', 'lawyer']


@app.route('/')
def view_jobs() -> str:
    return render_template('fancy_index.html', jobs=jobs)


@app.route('/post_job')
def post_job_view() -> str:
    job = request.args.get('title')
    if job:
        jobs.append(job)
        return f'Added job {job}!'
    else:
        return 'sorry, make sure you include title as a param'


@app.route('/job', methods=['POST'])
def post_job() -> str:
    return ''


app.run()
