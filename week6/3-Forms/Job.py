from datetime import datetime

class Job:
    def __init__(self, title: str, location: str):
        self.title = title
        self.location = location
        self.date_posted = datetime.now()