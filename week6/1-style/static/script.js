
function swapClass() {
    console.log('clicked');
    const element = document.getElementById('flashy');
    const classList = element.classList;
    const red_class = 'style-red';
    const blue_class = 'style-blue';

    if (classList.contains(blue_class)) {
        classList.remove(blue_class);
        classList.add(red_class);
    } else {
        classList.remove(red_class);
        classList.add(blue_class);
    }
}

document.getElementById('flash').addEventListener('click', swapClass);