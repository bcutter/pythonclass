from flask import Flask, render_template

app = Flask(__name__)

@app.route('/')
def style():
    return render_template('index2.html')

app.run()