class Myself:
    age = None

    # __init__
    def __init__(self, age):
        self.age = age

    def grow_older(self, years):
        self.age = self.age + years


me = Myself(31)

me.grow_older(1)

print(f'Age is now {me.age}')