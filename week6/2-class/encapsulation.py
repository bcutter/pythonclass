from pprint import pprint


class Printer:
    def print(self, text):
        pprint("This is some fancy printing!")
        pprint(text)

        # print(f'This is some fancy printing!\n{text}')


printer = Printer()
printer.print("hello world!")
