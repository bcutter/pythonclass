class Animal:
    def breathe(self):
        print("getting oxygen")


class Dog(Animal):
    def bark(self):
        print("woof")


class SiberianHusky(Dog):
    def awoo(self):
        print("awooooo")


rando_animal = Animal()

print("Animal Class:")
rando_animal.breathe()

print("\n\nDog Class")
dog_you_want_to_adopt = Dog()
dog_you_want_to_adopt.breathe()
dog_you_want_to_adopt.bark()

print("\n\nSiberianHusky Class")
cassius = SiberianHusky()
cassius.breathe()
cassius.bark()
cassius.awoo()
