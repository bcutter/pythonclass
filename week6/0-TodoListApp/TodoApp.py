from flask import Flask, request, render_template, redirect

app = Flask(__name__)

todos = list()

@app.route('/')
def view_todos() -> str:
    return render_template('index.html', todos=todos)

@app.route('/add')
def add_todo() -> str:
    todo = request.args.get('task')
    if todo:
        todos.append(todo)
        return redirect('/')
    else:
        return 'sorry, make sure you include title as a param'

@app.route('/finish/<int:id>')
def finish_todo(id):
    if len(todos) == 0 or id is None or id < 1 or id > len(todos):
        return f'could not remove {id}'
    else:
        todos.pop(id - 1)
        return redirect('/')

app.run()