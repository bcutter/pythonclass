vowels = {'a', 'e', 'i', 'o', 'u'}


def count_vowels(word: str):
    count_of_vowels = 0
    for letter in word:
        if letter in vowels:
            count_of_vowels += 1
    print(f'{word} has {count_of_vowels} vowels')


count_vowels('Onomatopoeia')
