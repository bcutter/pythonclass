from typing import Optional


def is_semi_tone(note_ord: int) -> bool:
    """
    Determines if a note is a tone or a semitone
    :param note_ord: the integer representation of a note
    :return: boolean
    """
    semi_tones = [ord('B'), ord('E')]
    return note_ord in semi_tones


def get_distance_between_notes(low_note_ord: int, high_note_ord: int) -> int:
    """
    Given two notes, in ascending order, determine the number of semitones between them
    :param low_note_ord: the lower of two notes
    :param high_note_ord: the higher of two notes
    :return: total semitones between two notes
    """
    distance = 0
    cur_note = low_note_ord
    while cur_note != high_note_ord:
        if is_semi_tone(cur_note):
            distance += 1
        else:
            distance += 2
        cur_note += 1
    return distance


def distance_between_notes(first_note: str, second_note: str) -> int:
    first_note_ord = ord(first_note)
    second_note_ord = ord(second_note)
    if first_note_ord < second_note_ord:
        return get_distance_between_notes(first_note_ord, second_note_ord)
    else:
        return get_distance_between_notes(second_note_ord, first_note_ord) * -1


def is_real_note(note):
    return ord('A') <= ord(note) <= ord('G')


def validate_input(vals: str) -> Optional[list[str]]:
    notes = vals.upper().split(' ')
    if len(notes) == 2 and is_real_note(notes[0]) and is_real_note(notes[1]):
        return notes
    return None


def run():
    while True:
        print('Input first and second note separated by a space (q to quit)')
        vals = input()
        if vals == 'q':
            quit()
        notes = validate_input(vals)
        if notes:
            first_note, second_note = notes
            distance = distance_between_notes(first_note, second_note)
            print(f'the distance between {first_note} and {second_note} is {distance} semitones')
        else:
            print('Invalid input')


run()
