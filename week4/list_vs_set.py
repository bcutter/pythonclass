import datetime
import random


def get_cur_timestamp():
    return int(datetime.datetime.now().timestamp() * 1000000)


def check_for_vals(collection) -> int:
    random_val_in = random.randint(0, len(collection))
    random_val_out = random_val_in + len(collection)
    start_time = get_cur_timestamp()
    if random_val_in in collection:
        pass
    if random_val_out not in collection:
        pass
    total_time = get_cur_timestamp() - start_time
    return total_time


def run_comparison(collection_size: int):
    test_list = list()
    test_set = set()
    for a in range(0, collection_size):
        test_list.append(a)
        test_set.add(a)

    list_time = check_for_vals(test_list)
    set_time = check_for_vals(test_set)
    print(f'List: {list_time} microseconds')
    print(f'Set:  {set_time} microseconds')


run_comparison(100000000)
