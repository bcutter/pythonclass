def get_older():
    global age
    age += 1


age = 20

get_older()
print(age)

tims_age = 15
jims_age = 55
lins_age = 30


def get_older_accurate(cur_age: int, years_progressed: int = 1) -> int:
    return cur_age + years_progressed


tims_age = get_older_accurate(tims_age)
print(tims_age)

jims_age = get_older_accurate(jims_age, 5)
print(jims_age)
