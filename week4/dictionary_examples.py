advertiser = {
    'id': 3,
    'name': 'African Swallow Delivery Service',
    'first_revenue_date': '2010-01-02',
    'industry': 'transportation'
}

print(advertiser['id'])

advertiser['last_revenue_date'] = '2021-04-12'

print(advertiser)

advertiser.pop('last_revenue_date')

print(advertiser)

for column_name, value in advertiser.items():
    print(f'{column_name}={value}')